# Czech translation for scilab
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the scilab package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: scilab\n"
"Report-Msgid-Bugs-To: <localization@lists.scilab.org>\n"
"POT-Creation-Date: 2013-04-16 17:44+0100\n"
"PO-Revision-Date: 2021-07-11 01:00+0000\n"
"Last-Translator: Stéphane Mottelet <Unknown>\n"
"Language-Team: Czech <cs@li.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Launchpad (build 1b66c075b8638845e61f40eb9036fabeaa01f591)\n"

#, c-format
msgid "%s: Wrong type for input arguments: Scalar values expected.\n"
msgstr "%s: Špatný typ vstupních argumentů: Očekávány skalární hodnoty.\n"

#, c-format
msgid "%s: An error occurred.\n"
msgstr "%s: Vyskytla se chyba.\n"

#, c-format
msgid "%s: Memory allocation error.\n"
msgstr "%s: Chyba při přidělování paměti.\n"

#, c-format
msgid "%s: Can not read input argument #%d.\n"
msgstr "%s: Nelze přečíst vstupní argument #%d.\n"

#, c-format
msgid "%s: No more memory.\n"
msgstr "%s: Není více paměti.\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: '%s' expected.\n"
msgstr "%s: Špatná hodnota vstupního argumentu #%d: Očekáváno '%s'.\n"

#, c-format
msgid "%s: Wrong size for input argument #%d: string expected.\n"
msgstr "%s: Špatná velikost vstupního argumentu #%d: Očekáván řetězec.\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: Must be > %d.\n"
msgstr "%s: Špatná hodnota vstupního argumentu #%d: Musí být > %d.\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: A real expected.\n"
msgstr "%s: Špatná hodnota vstupního argumentu #%d: Očekáváno reálné číslo.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: Integer or '%s' expected.\n"
msgstr ""
"%s: Špatný typ vstupního argumentu #%d: očekáváno celé číslo nebo '%s'.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A real scalar expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input argument(s): %d to %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Argument #%d: the scalar must be positive.\n"
msgstr ""

#, c-format
msgid "%s: Wrong value for input argument #%d: 's' expected.\n"
msgstr ""

#, c-format
msgid "%s: Argument #%d: Scalar (1 element) expected.\n"
msgstr ""

#, c-format
msgid "%s: Argument #%d: Must be in the interval [%d, %d].\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input arguments: %d or %d expected.\n"
msgstr "%s: Špatný počet vstupních argumentů: Očekávány %d nebo %d.\n"

msgid "Jan"
msgstr "Led"

msgid "Feb"
msgstr "Únor"

msgid "Mar"
msgstr "Bře"

msgid "Apr"
msgstr "Dub"

msgid "May"
msgstr "Kvě"

msgid "Jun"
msgstr "Čer"

msgid "Jul"
msgstr "Čvc"

msgid "Aug"
msgstr "Srp"

msgid "Sep"
msgstr "Zář"

msgid "Oct"
msgstr "Říj"

msgid "Nov"
msgstr "Lis"

msgid "Dec"
msgstr "Pro"

msgid "Mon  Tue  Wed  Thu  Fri  Sat  Sun"
msgstr "Pon  Úte  Stř  Čtv  Pát  Sob  Ned"

#, c-format
msgid "%s: Wrong type for input argument #%d: Real matrix expected.\n"
msgstr ""
"%s: Špatný typ vstupního argumentu #%d: Očekávána matice reálných čísel.\n"

#, c-format
msgid ""
"%s: Wrong size for input argument #%d: m*3 matrix or a m*6 matrix expected.\n"
msgstr ""
"%s: Špatná velikost vstupního argumentu #%d: očekávána matice m*3 nebo m*6.\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: %s must be between %d and %d.\n"
msgstr ""
"%s: Špatná hodnota vstupního argumentu #%d: %s musí být mezi %d a %d.\n"

msgid "Month"
msgstr "Měsíc"

msgid "Day"
msgstr "Den"

msgid "Hour"
msgstr "Hodina"

msgid "Minute"
msgstr "Minuta"

msgid "Second"
msgstr "Vteřina"

#, c-format
msgid "%s: Wrong type for input arguments.\n"
msgstr "%s: Špatný typ vstupních argumentů.\n"

#, c-format
msgid "%s: Wrong size for input arguments: Same size expected.\n"
msgstr "%s: Špatná velikost vstupních argumentů: Očekávána stejná velikost.\n"

#, c-format
msgid "%s: Wrong number of input argument.\n"
msgstr "%s: Špatný počet vstupních argumentů.\n"

#, c-format
msgid "%s: Wrong number of input argument: %d expected.\n"
msgstr "%s: Špatný počet vstupních argumentů: očekáváno %d.\n"

#, c-format
msgid "%s: Wrong number of input argument(s): %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong value for input argument #%d: An integer value expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong size for input arguments #%d and #%d: Same sizes expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: Real vector expected.\n"
msgstr "%s: Špatný typ vstupního argumentu #%d: Očekáván reálný vektor.\n"

#, c-format
msgid "%s: Wrong size for input argument #%d: Must be between %d and %d.\n"
msgstr "%s: Špatná velikost vstupního argumentu #%d: Musí být mezi %d a %d.\n"

#, c-format
msgid "%s: Wrong size for input arguments #%d and #%d: Same size expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong number of input arguments: %d to %d expected.\n"
msgstr ""

#, c-format
msgid "%s: Argument #%d: Integers in [1,12] expected.\n"
msgstr ""

#, c-format
msgid "%s: Argument #%d missing: Integers in [1,31] expected.\n"
msgstr ""

#, c-format
msgid "%s: Argument #%d: Integers in [1,31] expected.\n"
msgstr ""

#, c-format
msgid "%s: Arguments #%d to #%d: Same sizes expected.\n"
msgstr ""

#, c-format
msgid "%s: Argument #%d: Numbers or texts expected.\n"
msgstr ""

#
# File: modules/time/macros/weekday.sci, line: 99
# File: modules/time/macros/weekday.sci, line: 117
# File: modules/time/macros/weekday.sci, line: 138
# File: modules/time/macros/weekday.sci, line: 159
#, c-format
msgid "%s: Argument #%d: All dates must have the same format.\n"
msgstr ""

#
# File: modules/time/macros/weekday.sci, line: 165
#, c-format
msgid "%s: Argument #%d: Wrong dates format.\n"
msgstr ""

#
# File: modules/time/macros/weekday.sci, line: 169
#, c-format
msgid "%s: Argument #%d: Month indices expected in [1,12].\n"
msgstr ""

#
# File: modules/time/macros/weekday.sci, line: 173
#, c-format
msgid "%s: Argument #%d: Day indices expected in [1,31].\n"
msgstr ""

#, c-format
msgid "%s: Argument #%d: Single text word expected.\n"
msgstr ""

#
# File: modules/time/macros/weekday.sci, line: 199
#, c-format
msgid "%s: Argument #%d: Wrong langage code.\n"
msgstr ""

#
# File: modules/time/macros/weekday.sci, line: 204
#, c-format
msgid "%s: Argument #%d: Unknown option.\n"
msgstr ""

msgid "Friday"
msgstr "Pátek"

msgid "Monday"
msgstr "Pondělí"

msgid "Saturday"
msgstr "Sobota"

msgid "Sunday"
msgstr "Neděle"

msgid "Thursday"
msgstr "Čtvrtek"

msgid "Tuesday"
msgstr "Úterý"

msgid "Wednesday"
msgstr "Středa"

msgid "Fri"
msgstr "Pá"

msgid "Mon"
msgstr "Po"

msgid "Sat"
msgstr "So"

msgid "Sun"
msgstr "Ne"

msgid "Thu"
msgstr "Čt"

msgid "Tue"
msgstr "Út"

msgid "Wed"
msgstr "St"

#, c-format
#~ msgid ""
#~ "%s: Wrong type  for input argument #%d: Real constant matrix expected.\n"
#~ msgstr ""
#~ "%s: Špatný typ vstupního argumentu #%d: Očekávána reálná konstantní "
#~ "matice.\n"

#, c-format
#~ msgid "%s: Wrong value for input argument #%d: '%s' or '%s' expected.\n"
#~ msgstr ""
#~ "%s: Špatná hodnota vstupního argumentu #%d: Očekáváno \"%s\", nebo \"%s"
#~ "\".\n"
